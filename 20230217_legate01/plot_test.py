import pandas as pd

import plotly.graph_objects as go
import plotly.express as px
from plotly.subplots import make_subplots


def get_data():
    df = pd.DataFrame()

    for f in ['result/03c.csv', ]:
        df = pd.concat([df, pd.read_csv(f)])

    df['lib'] = ['numpy', 'cupy', 'cunumeric', 'cunumeric (dgx args)'] * (len(df) // 4)

    df['data scale (GB)'] = df['num_rows'] * 4 / 1e9
    df['throughput (GB/s)'] = df['data scale (GB)'] / df['time (s)']
    return df


def plot_throughput():

    df = get_data()

    y = 'time (s)'
    y = 'throughput (GB/s)'
    x = 'data scale (GB)'
    df['cat'] = df['lib'].astype(str) + ' '+ df['bench'].astype(str)

    df = df.sort_values(['lib', 'bench', 'num_rows'])


    fig = make_subplots(rows=1, cols=1, subplot_titles=[''], )

    f = px.colors.qualitative.Plotly
    f = [px.colors.sequential.Purples[7],
    px.colors.sequential.Purples[5],
    px.colors.sequential.Purples[4],
    px.colors.sequential.Purples[3],
    px.colors.sequential.Magenta[5],
    px.colors.sequential.Magenta[4],
    px.colors.sequential.Magenta[3],
    px.colors.sequential.Magenta[2],
         px.colors.sequential.Blues[7],
    px.colors.sequential.Blues[6],
    px.colors.sequential.Blues[5],
    px.colors.sequential.Blues[4],

        px.colors.sequential.Oranges[7],
    px.colors.sequential.Oranges[6],
    px.colors.sequential.Oranges[5],
    px.colors.sequential.Oranges[4],


         ]


    for n, c in enumerate(df['cat'].unique()):

        if 'cunumeric bench' in c:
            continue

        if 'numpy' in c:
            continue

        d = df[df['cat'] == c]
        fig.add_trace(go.Scatter(
            x=d[x],
            y=d[y],
            mode='markers+lines',
            #mode='markers',
            name=c,
            marker=dict(
                size=10,
                # color='white',  # f[n % len(f)],
                color='rgba(255, 255, 250, 0.0)',
                #color=f[n],
                # symbol=fs[n],
                line=dict(color=f[n%len(f)], width=3),
            ),
            line=dict(color=f[n%len(f)]),
        ))

    fig.update_layout(title={'text': "",
                             'y': 0.9,
                             'x': 0.3, })
    fig.update_layout(yaxis_title=y,
                      xaxis_title=x)

    fig.update_xaxes(type="log")
    fig.update_yaxes(type="log")
    fig.show()


def plot_outcome():






    df = get_data()

    df['cat'] = df['lib'] + ' ' + df['bench']

    df = df.sort_values(['cat', 'num_rows'])

    img_rgb = []
    for y in df['cat'].unique():
        row = []
        for x in df['num_rows'].unique():

            d = df[(df['cat'] == y) & (df['num_rows'] == x)]

            if pd.notnull(d['time (s)'].iloc[0]):
                row.append([0,200,0])
            elif d['timeout'].iloc[0]:
                row.append([90, 90, 90])
            else:
                row.append([200, 0, 0])

        img_rgb.append(row)


    fig = go.Figure(go.Image(z=img_rgb))

    fig.update_xaxes(
        ticktext=df['data scale (GB)'].round(2).unique().tolist(),
        tickvals=list(range(len(df['data scale (GB)'].unique().tolist()))),
    )

    fig.update_yaxes(
        ticktext=df['cat'].unique().tolist(),
        tickvals=list(range(len(df['cat'].unique().tolist()))),
    )
    fig.update_layout(xaxis_title='data scale (GB)')

    fig.show()




if __name__ == '__main__':

    plot_throughput()

    #plot_outcome()