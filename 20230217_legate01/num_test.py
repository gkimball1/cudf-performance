import sys
import nvtx
import time 

if sys.argv[1] == 'cupy':
    import cupy as np
    print("Use cupy")
elif sys.argv[1] == 'cunumeric':
    import cunumeric as np
    print("Use cunumeric")
elif sys.argv[1] == 'numpy':
    import numpy as np
    print("Use numpy")
else:
    raise Exception("Unknown arg: " + sys.argv[1])


try:
    benchmark = sys.argv[2]
except:
    benchmark = '0'

try:
    nr = int(sys.argv[3])
except:
    nr = 400_000


def bench_binop():
    data1 = np.random.randint(0, 10, nr)
    data2 = np.random.randint(0, 100, nr)
    with nvtx.annotate(sys.argv[1] + ' ' + benchmark, color="yellow"):
        t0 = time.time()
        a = data1 + data2
        print(a[0])
        t1 = time.time()
    print(t1-t0)


def bench_extract():
    data1 = np.random.randint(0, 10, nr)
    data2 = np.random.randint(0, 100, nr)

    with nvtx.annotate(sys.argv[1] + ' ' + benchmark, color="yellow"):
        t0 = time.time()
        a = np.extract(data1==data1[0], data2)
        print(a[0])
        t1 = time.time()
        print(t1-t0)


def bench_unique():
    data1 = np.random.randint(0, 1_000_000, nr)    

    with nvtx.annotate(sys.argv[1] + ' ' + benchmark, color="yellow"):
        t0 = time.time()
        a = np.unique(data1)
        print(a[0])
        t1 = time.time()
        print(t1-t0)


def bench_sort():
    data1 = np.random.randint(0, 1_000_000, nr)    

    with nvtx.annotate(sys.argv[1] + ' ' + benchmark, color="yellow"):
        t0 = time.time()
        a = np.sort(data1)
        print(a[0])
        t1 = time.time()
        print(t1-t0)



if benchmark == 'bench_binop':
    bench_binop()
elif benchmark =='bench_extract':
    bench_extract()
elif benchmark == 'bench_unique':
    bench_unique()
elif benchmark == 'bench_sort':
    bench_sort()


'''
python num_test.py numpy
python num_test.py cupy
legate num_test.py cunumeric



legate --gpus=6 num_test.py cunumeric

/nfs/nsight-systems-2022.5.1/bin/nsys profile -t nvtx,cuda,osrt -f true --cuda-memory-usage=true --gpu-metrics-device=0 --output=/nfs/20230217_legate01/legate_prof_02 legate --gpus=6 num_test.py cunumeric

/nfs/nsight-systems-2022.5.1/bin/nsys profile -t nvtx,cuda,osrt -f true --cuda-memory-usage=true --gpu-metrics-device=0 --output=/nfs/20230217_legate01/legate_prof_03 python num_test.py cupy
'''

