


import subprocess
import os
import pandas as pd
import time
import nvtx


prof = [
    '/nfs/nsight-systems-2022.5.1/bin/nsys',
    'profile',
    '-t nvtx,cuda,osrt',
    '-f true',
    '--cuda-memory-usage=true', 
    '--gpu-metrics-device=0',
    '--output=/nfs/20230217_legate01/legate_prof_02',
]

df = pd.DataFrame()


bench_list = [
    'bench_binop', 'bench_extract','bench_unique','bench_sort'
]


for nr_factor in range(20,35):
    for bench in ['bench_binop', 'bench_extract','bench_unique','bench_sort']:
        nr = 2**nr_factor
        for cmd in (
            ['python', 'num_test.py', 'numpy', str(bench), str(nr)],
            ['python', 'num_test.py', 'cupy', str(bench), str(nr)],
            ['legate', 'num_test.py', 'cunumeric', str(bench), str(nr)],
            ['legate', '--cpus=16', '--gpus=4', '--sysmem=100000', '--fbmem=15000', 'num_test.py', 'cunumeric', str(bench), str(nr)]
        ):

            timeout_limit = 600
            timeout = False

            stderr = ''
            stdout = ''

            try:
                with nvtx.annotate(cmd[-2] + ' process', color="orange"):
                    t0 = time.time()
                    out = subprocess.run(cmd, capture_output=True, timeout=timeout_limit)
                    t1 = time.time()
                
                stderr = out.stderr.decode('utf-8')
                stdout = out.stdout.decode('utf-8')
                print(cmd, stderr[:100])
            except subprocess.TimeoutExpired as err:
                t1 = time.time()
                timeout = True  
                                       


            x = len(df)
            df.at[x, 'lib'] = cmd[-3]
            df.at[x, 'bench'] = bench
            df.at[x, 'cmd'] = ' '.join(cmd)
            df.at[x, 'timeout'] = timeout
            df.at[x, 'timeout_limit'] = timeout_limit

            df.at[x, 'num_rows'] = nr
            df.at[x, 'stderr'] = stderr

            try:
                df.at[x, 'time (s)'] = float(stdout.split('\n')[-2])
                print( df.at[x, 'time (s)'])
            except:
                pass
            df.at[x, 'e2e time (s)'] = t1-t0

        df.to_csv('result/03c.csv')

    
print(df)


'''

'''